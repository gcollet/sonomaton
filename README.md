# Le Sonomaton

Le projet sonomaton a été développé au sein de l'atelier partagé. Il s'agit
d'un photomaton dans lequel, au lieu d'enregistrer une photo, on enregistre un
son. A la sortie, au lieu d'obtenir une photo, on obtient des QR code autocollants
qui renvoient vers le son en ligne.

## Scripts de test
Je teste des idées avec des scripts, voici un résumé avec les sources :

### read_keyboard.py
un script pour lire les touches du clavier dans une boucle infinie, récupéré sur https://www.jonwitts.co.uk/archives/896

### record.py
Un script qui enregistre le son (et le joue)

### monitor.py
Un script qui montre comment affecter une fonction callback sur une pin du GPIO
du raspberry pi. Si la pin change d'état (EITHER_EDGE, RISING_EDGE, FALLING_EDGE.)
la fonction s'exécute.

Je copie ici la doc sur callback http://abyz.me.uk/rpi/pigpio/python.html#callback

```
callback(user_gpio, edge, func)
Calls a user supplied function (a callback) whenever the specified GPIO edge is detected.

Parameters

user_gpio:= 0-31.
     edge:= EITHER_EDGE, RISING_EDGE (default), or FALLING_EDGE.
     func:= user supplied callback function.


The user supplied callback receives three parameters, the GPIO, the level, and the tick.

Parameter   Value    Meaning

GPIO        0-31     The GPIO which has changed state

level       0-2      0 = change to low (a falling edge)
                     1 = change to high (a rising edge)
                     2 = no level change (a watchdog timeout)

tick        32 bit   The number of microseconds since boot
                     WARNING: this wraps around from
                     4294967295 to 0 roughly every 72 minutes


If a user callback is not specified a default tally callback is provided which simply counts edges. The count may be retrieved by calling the tally function. The count may be reset to zero by calling the reset_tally function.

The callback may be cancelled by calling the cancel function.

A GPIO may have multiple callbacks (although I can't think of a reason to do so).

Example

def cbf(gpio, level, tick):
   print(gpio, level, tick)

cb1 = pi.callback(22, pigpio.EITHER_EDGE, cbf)

cb2 = pi.callback(4, pigpio.EITHER_EDGE)

cb3 = pi.callback(17)

print(cb3.tally())

cb3.reset_tally()

cb1.cancel() # To cancel callback cb1.
```
## Autre lien

https://medium.com/@rxseger/interrupt-driven-i-o-on-raspberry-pi-3-with-leds-and-pushbuttons-rising-falling-edge-detection-36c14e640fef

## Boutons et états du programme

**Boutons :**
* Rouge : démarre et arrête l'enregistrement
* Bleu  : relecture du dernier enregistrement
* Vert  : validation du dernier enregistrement

**Etats :**
* *waiting* : attend que l'utilisateur lance un enregistrement.
* *recording* : enregistrement du son.
* *recorded* : le son est enregistré mais pas encore validé.
* *playing* : le dernier son enregistré est joué.
* *saving* : le dernier enregistrement est écrit dans un fichier.

L'état *saving* est transitoire et passe automatiquement à *waiting* une fois le fichier enregistré.

**Changements d'état : vision états**
* en état *waiting* :
  * bouton rouge : démarre enregistrement -> *recording*
  * bouton bleu : rien
  * bouton vert : rien
* en état *recording* :
  * bouton rouge : arrête l'enregistrement -> *recorded*
  * bouton bleu : rien
  * bouton vert : rien
  * Après 5 minutes d'enregistrement -> *recorded*
* en état *recorded* :
  * bouton rouge : démarre l'enregistrement -> *recording*
  * bouton bleu : lit l'enregistrement -> *playing*
  * bouton vert : Valide l'enregistrement -> *saving*
* en état *playing* :
  * bouton rouge : rien
  * bouton bleu : Arrête la lecture du son -> *recorded*
  * bouton vert : rien
  * Après lecture complète de l'enregistrement -> *recorded*
* en état *saving* :
  * bouton rouge : rien
  * bouton bleu : rien
  * bouton vert : rien
  * Après enregistrement du fichier -> *waiting*

**Changements d'état : vision boutons**
* Bouton rouge :
  * *waiting* -> *recording*
  * *recording* -> *recorded*
  * *recorded* -> *recording*
* Bouton bleu :
  * *recorded* -> *playing*
  * *playing* -> *recorded*
* Bouton vert :
  * *recorded* -> *waiting*
