#!/usr/bin/env python3

import keyboard, time
import pyaudio, wave
from random import choice
from string import ascii_uppercase

# Parametres pour l'enregistrement et la lecture des sons
BUFFER_SIZE = 1024
REC_SECONDS = 300
RATE = 44100
CHANNELS = 1
FORMAT = pyaudio.paInt16
SAMPLE_WIDTH = 0

# Variables globales
state = 'waiting' # Etat du programme
nb_record = 0     # Nombre d'enregistrements effectués
max_record = 3    # Nombre maximum d'enregistrements possibles
data_frames = []

# Définition de la fonction d'enregistrement
def record_data_frames():
    global data_frames
    global state
    global SAMPLE_WIDTH
    # Init sound stream
    pa = pyaudio.PyAudio()
    # Definition du stream
    stream = pa.open(
        format = FORMAT,
        input = True,
        channels = CHANNELS,
        rate = RATE,
        frames_per_buffer = BUFFER_SIZE
    )
    # Run recording
    data_frames = []
    nb_frame = 0
    max_nb_frame = int(RATE / BUFFER_SIZE * REC_SECONDS)
    while (state == 'recording' and nb_frame < max_nb_frame):
        data = stream.read(BUFFER_SIZE)
        data_frames.append(data)
        nb_frame += 1
    stream.stop_stream()
    stream.close()
    SAMPLE_WIDTH = pa.get_sample_size(FORMAT)
    pa.terminate()

# Définition de la fonction de lecture
def play_data_frames():
    global data_frames
    global state
    # instantiate PyAudio (1)
    p = pyaudio.PyAudio()
    # open stream (2)
    stream = p.open(
        format = FORMAT,
        channels = CHANNELS,
        rate = RATE,
        output=True
    )
    # play stream (3)
    for data in data_frames:
        stream.write(data)
        if (state != 'playing'):
            break
    # stop stream (4)
    stream.stop_stream()
    stream.close()
    # close PyAudio (5)
    p.terminate()

# Définition de la fonction de sauvegarde du fichier
def save_data_frames():
    global data_frames
    global SAMPLE_WIDTH
    global all_names
    global done_names
    WAV_FILENAME = all_names[0]
    all_names.remove(WAV_FILENAME)
    done_names.append(WAV_FILENAME)
    wf = wave.open(WAV_FILENAME+'.wav', 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(SAMPLE_WIDTH)
    wf.setframerate(RATE)
    wf.writeframes(b''.join(data_frames))
    wf.close()
    print ('\rFile written in ' + WAV_FILENAME + '.wav')

# Définition des callbacks pour les interruptions boutons
def cbf_red (event):
    global state
    global nb_record
    global max_record
    if (state == 'waiting'):
        state = 'recording'
        print ('\rLumière rouge                                                      ')
    elif (state == 'recording'):
        state = 'recorded'
        nb_record += 1
        print ('\rLumière bleue                                                      ')
        print ('\rAttempt ' + str(nb_record))
    elif (state == 'recorded'):
        if (nb_record == max_record):
            state = 'waiting'
            print ('Only 3 try                                                      ')
            print('\nLumière verte                                                      ')
            nb_record = 0
        else :
            state = 'recording'
            print ('\rLumière rouge                                                      ')

def cbf_green (event):
    global state
    global nb_record
    if (state == 'recorded'):
        state = 'saving'
        print ('\n\nLumière verte                                                      ')
        nb_record = 0

def cbf_blue (event):
    global state
    global nb_record
    if (state == 'recorded'):
        state = 'playing'
    elif (state == 'playing'):
        state = 'recorded'

def cbf_quit (event):
    global state
    state = 'quit'

# Application des callbacks aux touches clavier
# 15 = touche 'r'
# 9  = touche 'v'
# 35 = touche 'p'
keyboard.on_press_key(15, cbf_red)
keyboard.on_press_key(9,  cbf_green)
keyboard.on_press_key(35, cbf_blue)
keyboard.on_press_key(0,  cbf_quit)

# Prépare les listes de nom de fichier + prochain nom de fichier
#  1- lit les noms aléatoires dans list_alea.txt -> tous les noms possibles
all_names = []
with open('list_alea.txt','r') as f:
    for name in f:
        all_names.append(name.rstrip())
#  2- lit les noms aléatoires dans list_done.txt -> noms déjà utilisés
done_names = []
with open('list_done.txt', 'r') as f:
    for name in f:
        if (all_names[0] == name.rstrip()):
            all_names.remove(name.rstrip())
        done_names.append(name.rstrip())

# Début de la boucle de jeu
try:
    print ('\rSystem on -> waiting\n')
    print ('\rLumière verte                                                      ')
    while True:
# WAITING   ###################################################################
        if (state == 'waiting'):
            print ('\rPress r to start recording                             ', end='\r')
# RECORDING ###################################################################
        elif (state == 'recording'):
            print ('\rPress r to stop recording                              ', end='\r')
            record_data_frames()
# RECORDED  ###################################################################
        elif (state == 'recorded'):
            print ('\rPress r to start recording, p to play or v to validate ', end='\r')
# PLAYING   ###################################################################
        elif (state == 'playing'):
            print ('\rPress p to stop playing                                ', end='\r')
            play_data_frames()
            state = 'recorded'
# SAVING   ####################################################################
        elif (state == 'saving'):
            save_data_frames()
            state = 'waiting'
# QUIT   ######################################################################
        elif (state == 'quit'):
            print ('\rWrite done files')
            with open('list_done.txt', 'w') as f:
                for name in done_names:
                    f.write(name + '\n')
            print ('\rQuit                                                     \n')
            exit(0)
        time.sleep(0.1)
# On capture le Ctrl+c pour quitter proprement
except KeyboardInterrupt:
    print ('\rWrite done files')
    with open('list_done.txt', 'w') as f:
        for name in done_names:
            f.write(name + '\n')
    print ('\rQuit\n')


# Vieux code qui sert à trouver le scan_code d'une touche pour lui
# affecter une callback
# def cbf (event):
#     print ("code = " + str(event.scan_code))
#
# keyboard.hook(cbf)
