#!/usr/bin/env python3

import sys
from time import sleep
import pyaudio
import wave
from gpiozero import LED
from gpiozero import Button
from random import choice
from string import ascii_uppercase
from datetime import datetime

# Parametres pour l'enregistrement et la lecture des sons
# avec pyaudio et wave -> ToDo passer en mp3
BUFFER_SIZE = 512
REC_SECONDS = 900
RATE = 44100
CHANNELS = 1
FORMAT = pyaudio.paInt16
SAMPLE_WIDTH = 0

# Variables globales
state = 'waiting' # Etat du programme
data_frames = []  # Contient le son à enregistrer

# Définition de la fonction générant un nom aléatoire de 12 lettres
def gen_alea():
	return ''.join(choice(ascii_uppercase) for i in range(12))

# Définition de la fonction d'enregistrement
def record_data_frames():
	global data_frames
	global state
	global SAMPLE_WIDTH
	# Init sound stream
	pa = pyaudio.PyAudio()
	# Definition du stream
	stream = pa.open(
		format = FORMAT,
		input = True,
		channels = CHANNELS,
		rate = RATE,
		frames_per_buffer = BUFFER_SIZE
	)
	# Run recording
	data_frames = []
	nb_frame = 0
	max_nb_frame = int(RATE / BUFFER_SIZE * REC_SECONDS)
	while (state == 'recording' and nb_frame < max_nb_frame):
		data = stream.read(BUFFER_SIZE, exception_on_overflow=False)
		data_frames.append(data)
		nb_frame += 1
	stream.stop_stream()
	stream.close()
	SAMPLE_WIDTH = pa.get_sample_size(FORMAT)
	pa.terminate()

# Définition de la fonction de lecture
def play_data_frames():
	global data_frames
	global state
	# instantiate PyAudio (1)
	p = pyaudio.PyAudio()
	# open stream (2)
	stream = p.open(
		format			= FORMAT,
		channels          = CHANNELS,
		rate			  = RATE,
		output			= True,
		frames_per_buffer = 1
	)
	# play stream (3)
	for data in data_frames:
		stream.write(data)
		if (state != 'playing'):
			break
	# stop stream (4)
	stream.stop_stream()
	stream.close()
	# close PyAudio (5)
	p.terminate()

# Définition de la fonction de sauvegarde du fichier
def save_data_frames():
	global data_frames
	global SAMPLE_WIDTH
	mytoday = datetime.today()
	WAV_FILENAME = str(mytoday.year)+"-"+str(mytoday.month)+"-"+str(mytoday.day)+"-"+str(mytoday.hour)+"-"+str(mytoday.minute)+"-"+str(mytoday.second)
	wf = wave.open('/home/pi/sonomaton/'+WAV_FILENAME+'.wav', 'wb')
	wf.setnchannels(CHANNELS)
	wf.setsampwidth(SAMPLE_WIDTH)
	wf.setframerate(RATE)
	wf.writeframes(b''.join(data_frames))
	wf.close()
	data_frames = []
	print ('\rFile written in ' + WAV_FILENAME + '.wav')

# Définition des callbacks pour les interruptions boutons
def cbf_red ():
	global state
	if (state == 'waiting' or state == 'playing'):
		state = 'recording'
	elif (state == 'recording'):
		state = 'recorded'
	elif (state == 'recorded'):
		state = 'recording'
	
def cbf_blue ():
	global state
	if (state == 'recorded'):
		state = 'playing'
	elif (state == 'playing'):
		state = 'recorded'

def cbf_green ():
	global state
	if (state == 'recorded'):
		state = 'saving'

def cbf_quit ():
	global state
	state = 'quit'

# Pin number definition
PIN_GREEN_LIGHT = 17
PIN_BLUE_LIGHT = 27
PIN_RED_LIGHT = 22

PIN_BLACK_BTN = 18
PIN_GREEN_BTN = 12
PIN_BLUE_BTN = 6
PIN_RED_BTN = 5

# Définition des LED
red_led = LED(PIN_RED_LIGHT)
blue_led = LED(PIN_BLUE_LIGHT)
green_led = LED(PIN_GREEN_LIGHT)

# Définition des boutons
red_btn = Button(PIN_RED_BTN)
blue_btn = Button(PIN_BLUE_BTN)
green_btn = Button(PIN_GREEN_BTN)
black_btn = Button(PIN_BLACK_BTN)

# Associe l'action d'appui sur chaque bouton à la fonction callback du bouton
red_btn.when_pressed = cbf_red
blue_btn.when_pressed = cbf_blue
green_btn.when_pressed = cbf_green
black_btn.when_pressed = cbf_quit

prev_state='waiting'

# Début de la boucle de jeu
try:
	print('waiting')
	red_led.on()
	blue_led.on()
	green_led.on()
	sleep(5)
	while True:
		if (not state == prev_state):
			prev_state = state
			print (state)
# WAITING   ###################################################################
		if (state == 'waiting'):
			red_led.off()
			blue_led.off()
			green_led.off()
# RECORDING ###################################################################
		elif (state == 'recording'):
			red_led.on()
			blue_led.off()
			green_led.off()
			record_data_frames()
# RECORDED  ###################################################################
		elif (state == 'recorded'):
			red_led.off()
			blue_led.off()
			green_led.off()
# PLAYING   ###################################################################
		elif (state == 'playing'):
			red_led.off()
			blue_led.on()
			green_led.off()
			play_data_frames()
			state = 'recorded'
# SAVING   ####################################################################
		elif (state == 'saving'):
			red_led.off()
			blue_led.off()
			green_led.on()
			save_data_frames()
			sleep(1)
			state = 'waiting'
# QUIT   ######################################################################
		elif (state == 'quit'):
			print ('\rQuit\n')
			red_led.off()
			blue_led.off()
			green_led.off()
			exit(0)

# On capture le Ctrl+c pour quitter proprement
except KeyboardInterrupt:
	red_led.off()
	blue_led.off()
	green_led.off()
	print ('\rQuit\n')
